package actions

import models.User
import services.UserService

class CreateUser {
    val service: UserService = UserService();
    fun execute(user: User):User? {
        return service.CreateUser(user);
    }
}