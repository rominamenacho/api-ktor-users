package actions

import models.User
import services.UserService

class FindAllUsers {

    val service:UserService = UserService();

    fun execute():List<User> {
        return service.FindAll();
    }
}