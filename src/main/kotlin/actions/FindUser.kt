package actions

import models.User
import services.UserService

class FindUser {

    val service: UserService = UserService();

    fun execute(email:String):User? {
        return service.FindUser(email);
    }
}