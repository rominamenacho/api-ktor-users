package com.example

import io.ktor.server.engine.*
import io.ktor.server.netty.*
import com.example.plugins.*

fun main() {
    embeddedServer(Netty, port = System.getenv("PORT").toInt(), host = "0.0.0.0") {//System.getenv("PORT").toInt()
        configureHTTP()
        configureSerialization()
        configureSecurity()
        configureRouting()
    }.start(wait = true)
}
