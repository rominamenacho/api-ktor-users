package com.example.plugins

import io.ktor.server.routing.*
import io.ktor.server.application.*
import routes.userRouting

fun Application.configureRouting() {

    routing {
        userRouting()
    }
}
