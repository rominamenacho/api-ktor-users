package databases


import models.User
import org.ktorm.database.Database
import org.ktorm.dsl.*
import org.ktorm.entity.firstOrNull
import org.ktorm.entity.sequenceOf
import org.ktorm.entity.toList

class DataBaseManager {

    private val hostname ="sql.freedb.tech"
    private val port= "3306"
    private val userName = "freedb_topic"
    private val password = "!SJ7pR9Sxzj7tUd"
    private val databaseName= "freedb_topictwister"
    private val ktormDatabase:Database
/*probar
* SERVER=911nuebus.com(200.58.119.223) ; PORT=3309 ; USER=test_app ; PWD=user1234 ; DATABASE=test_app
* */
    init{

       val jdbcUrl= "jdbc:mysql://$hostname:$port/$databaseName?user=$userName&password=$password&useSSL=false"
        ktormDatabase=Database.connect(jdbcUrl)
    }
    fun getAllUsers(): List<DBUserEntity> {
        return ktormDatabase.sequenceOf(DBUserTable)
            .toList()
    }

    fun getUserByEmail(email:String):DBUserEntity?{
        return ktormDatabase.sequenceOf(DBUserTable)
            .firstOrNull{ it.email eq email }
    }

   /* fun getUserById(id:Int):DBUserEntity?{
        return ktormDatabase.sequenceOf(DBUserTable)
            .firstOrNull{ it.id eq id }
    }*/

    fun createUser(user: User):User?{

        val insertedId=ktormDatabase.insertAndGenerateKey(DBUserTable){
         //  set(DBUserTable.id, null)///ver si funciona
            set(DBUserTable.email,user.email)
            set(DBUserTable.name,user.name)
        } as Int

        return User( user.name, user.email)
    }

   /* fun updateUser(id:Int, name:String):Boolean{
        val updateRows=  ktormDatabase.update(DBUserTable){
            set(DBUserTable.name, name)
            where{
                it.id eq id
            }
        }
        return updateRows > 0
    }

    fun deleteUser(id:Int):Boolean{
        val deletedRows=ktormDatabase.delete(DBUserTable){
            it.id eq id
        }
        return  deletedRows>0
    }*/


}

