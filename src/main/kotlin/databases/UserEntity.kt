package databases

import org.ktorm.entity.Entity
import org.ktorm.schema.Table
import org.ktorm.schema.int
import org.ktorm.schema.varchar

object DBUserTable: Table<DBUserEntity> ("users"){
    val id=int("id").primaryKey().bindTo { it.id }
    val email= varchar("email").bindTo { it.email }
    val name=varchar("name").bindTo {  it.name }
}

interface DBUserEntity:Entity<DBUserEntity>{
    companion object : Entity.Factory<DBUserEntity>()

    val id:Int
    val email:String
    val name:String

}