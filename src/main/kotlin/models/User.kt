package models

import kotlinx.serialization.Serializable




@Serializable
class User(val name:String, val email:String )