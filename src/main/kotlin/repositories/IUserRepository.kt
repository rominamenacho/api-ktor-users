package repositories

import models.User

interface IUserRepository {
    fun getAllUsers():List<User>
    fun getUser(email:String):User?
    fun createUser(user:User):User?
}