package repositories

import models.User

class UserRepositoryInMemory:IUserRepository {
    val userStorage= mutableListOf<User>(
        User("nico1","abc@gmail.com"),
        User("nico2","abc2@gmail.com"),
        User("nico3","abc3@gmail.com"),
    )

    override fun getAllUsers(): List<User> {
        return userStorage
    }

    override fun getUser(email: String): User? {
        return userStorage.find { it.email==email }
    }

    override fun createUser(user:User): User? {
        userStorage.add(user)
        return user
    }
}