package repositories

import databases.DataBaseManager
import models.User


class UserRepositoryMySQL:IUserRepository {

    private val database=DataBaseManager()

    override fun getAllUsers(): List<User> {
        return database.getAllUsers()
            .map{ User(it.name,it.email) }
    }

    override fun getUser(email: String): User? {
        return database.getUserByEmail(email)
            ?.let { User( it.name,it.email) }

    }

    override fun createUser(user: User): User? {
        return database.createUser(user)
    }
}