package routes

import actions.CreateUser
import actions.FindAllUsers
import actions.FindUser
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import models.User
import repositories.IUserRepository
import repositories.UserRepositoryMySQL

fun Route.userRouting(){
    route("/user"){
        get{
            val usecase:FindAllUsers = FindAllUsers();
            var allUsers=usecase.execute();
            if(allUsers.isNotEmpty()){
                call.respond(allUsers)
            }else{
                call.respondText ("No user found", status= HttpStatusCode.NotFound )
            }
        }
        get("{email}"){

            val usecase:FindUser = FindUser();
            val email=call.parameters["email"]?:
        return@get call.respondText("Missing or malformed email", status = HttpStatusCode.BadRequest)
        val user =usecase.execute(email)?:return@get call.respondText("No user with email $email",
        status = HttpStatusCode.NotFound)
        call.respond(user)
        }
        post{
            val createUseCase:CreateUser = CreateUser();
            val findUseCase:FindUser = FindUser();

            val user=call.receive<User>()

            findUseCase.execute(user.email)?:createUseCase.execute(user)

            call.respond(user)
        }
    }
}