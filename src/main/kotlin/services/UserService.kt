package services

import models.User
import repositories.IUserRepository
import repositories.UserRepositoryMySQL

class UserService {

    val repository: IUserRepository = UserRepositoryMySQL()


    fun  FindAll():List<User> {
        return repository.getAllUsers();
    }

    fun  FindUser(email:String):User? {
        return repository.getUser(email);
    }

    fun CreateUser(user: User):User? {
        return repository.createUser(user);
    }


}